# Bonker-Bots-Battle

Copyright Kris Occhipinti 2023-07-13

(https://filmsbykris.com)

License GPLv3

# Sprites - tiles
- Ansimuz
Artwork created by Luis Zuno (@ansimuz)
https://www.patreon.com/posts

LICENSE:
You may use these assets in personal or commercial projects. You can modify these assets to suit your needs. You can re-distribute the file.
Credit no required but appreciated it.
____________________

LINKS
Twitter @ansimuz
Support my work at Patreon https://www.patreon.com/ansimuz
Buy my stuff https://ansimuz.itch.io/
Get more Free Assetslike these at: http://www.ansimuz.com
____________________

- Freedoom
Some Artwork and other assests are from the Freedoom project.
https://freedoom.github.io/about.html
Freedoom contains hundreds of original textures, sound effects and music tracks that can be reused royalty-free by Doom level authors and other independent game developers.

Freedoom is liberally licensed under the BSD license - all that is required is that you include a short copyright statement that credits the Freedoom project.

- BigMack
some gun sprites are from:
https://bigmack.itch.io/wwii-mega-gun-pack
Licence: Free to use personally and commercially. Credit is not necessary but it is appreciated

- PancInteractive
Food Sprites
https://opengameart.org/content/food-items-from-crosstown-smash
http://creativecommons.org/licenses/by/3.0/

# MUSIC
Music by teknoaxe
licensed under a Creative Commons Attribution 4.0 International License
https://creativecommons.org/licenses/by/4.0/
https://teknoaxe.com/Link_Code_3.php?q=1946&genre=Rock
https://teknoaxe.com/Link_Code_3.php?q=1958&genre=Rock
https://teknoaxe.com/Link_Code_3.php?q=1969&genre=Rock
https://teknoaxe.com/Link_Code_3.php?q=1991

