extends Control

onready var score_board = $score_board
onready var middle_msg = $middle_msg

func _ready():
	pass # Replace with function body.


func _process(_delta):
	var players = get_tree().get_nodes_in_group("players")
	var text = ""
	for i in players.size():
		text += "Player #" + str(i+1) + ":" + str(players[i].kills)
		text += "   "
	score_board.text = text

func end_game():
	get_tree().change_scene("res://scenes/Title_Menu.tscn")
