extends Node

onready var music_bus := AudioServer.get_bus_index("music")
var save_path = "user://music_volume.save"

func _ready():
	load_saved()

func _process(_delta):
	var volume_up = Input.is_joy_button_pressed(0,Controls.buttons.MUSIC_UP)
	var volume_down = Input.is_joy_button_pressed(0,Controls.buttons.MUSIC_DOWN)
	var steps = 0.01
	if volume_up:
		change_music_volume(steps)
	elif volume_down:
		change_music_volume(-steps)

func change_music_volume(amount):
	var volume = db2linear(AudioServer.get_bus_volume_db(music_bus))
	volume += amount
	
	#Volume Limits
	if volume < 0.001:
		volume = 0.001
	elif volume > 2:
		volume = 2
	
	AudioServer.set_bus_volume_db(music_bus, linear2db(volume))
	save(volume)
	
func save(volume):
	var file = File.new()
	file.open(save_path, File.WRITE)
	file.store_string(str(volume))
	file.close()
	
func load_saved():
	var file = File.new()
	if !file.file_exists(save_path):
		return
		
	file.open(save_path, File.READ)
	var volume = file.get_as_text()
	volume = volume.to_float()
	AudioServer.set_bus_volume_db(music_bus, linear2db(volume))
	file.close()
