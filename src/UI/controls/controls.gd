extends Node

var buttons = {
	"LEFT":JOY_DPAD_LEFT,
	"RIGHT":JOY_DPAD_RIGHT,
	"UP":JOY_DPAD_UP,
	"DOWN":JOY_DPAD_DOWN,
	"JUMP":JOY_DS_B,
	"SHOOT":JOY_DS_A,
	"CHANGE_WEAPON": JOY_DS_X,
	"START": JOY_START,
	"CHANGE_MUSIC": JOY_SELECT,
	"MUSIC_UP" : 16,
	"MUSIC_DOWN"  : 17
}


var save_path = "user://controls.save"

#if start button held for 3 seconds setup menu will appear
var press_time = 0
func _ready():
	load_controls()
	
func _process(delta):
	var key = Input.is_action_just_pressed("setup_menu")
	var button = Input.is_joy_button_pressed(0,Controls.buttons.START)
	if button:
		press_time += delta
	else:
		press_time = 0

	if press_time > 3 || key:
		get_tree().change_scene("res://UI/controls/Controls_Setup.tscn")

func save():
	var file = File.new()
	file.open(save_path, File.WRITE)
	file.store_string(to_json(buttons))
	file.close()

func load_controls():
	var file = File.new()
	if !file.file_exists(save_path):
		return
		
	file.open(save_path, File.READ)
	var buttons_tmp = parse_json(file.get_as_text())
	file.close()
	
	if buttons_tmp.size() != buttons.size():
		get_tree().change_scene("res://UI/controls/Controls_Setup.tscn")
	else:
		buttons = buttons_tmp
	
func _input(event):
	if event is InputEventJoypadButton && event.is_pressed():
		return
		#print(event.button_index)
