extends Button

export var start_focus = false
export var map = "res://maps/map_01.tscn"

func _ready():
	if start_focus:
		grab_focus()
		

func _on_button_pressed():
	var fader = get_tree().get_nodes_in_group("fader")[0]
	fader.play("fade")
	$AnimationPlayer.play("load")

func _on_button_focus_entered():
	$select.stop()
	$focus.play()

func load_map():
	get_tree().change_scene(map)


