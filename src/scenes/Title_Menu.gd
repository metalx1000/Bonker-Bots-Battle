extends Node2D

func _unhandled_input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()
	elif event.is_pressed():
		get_tree().change_scene("res://UI/maps_selection/Map_Selection.tscn")
		
func credit_scene():
	get_tree().change_scene("res://scenes/Credits.tscn")
