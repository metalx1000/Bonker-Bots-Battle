extends Area2D

export var rotate_speed = .02
export var speed = 20
var damage = 10

var leftright 
var parent

func _ready():
	randomize()
	leftright = rand_range(-10,10)
	$Sprite.frame = rand_range(0,8)

func _process(delta):
	rotate(rotate_speed)
	position.y -= delta * speed
	position.x += leftright * delta
	
func _on_fart_body_entered(body):
	if body.is_in_group("players")&& body != parent:
		body.fart_damage.play("fart_damage")
		body.attacker = parent
