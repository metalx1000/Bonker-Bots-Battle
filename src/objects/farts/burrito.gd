extends KinematicBody2D

export (int) var gravity = 1000
var velocity = Vector2()

func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)

func _on_Area2D_body_entered(body):
	if body.has_method("fart_start"):
		body.fart()
		body.fart_start()

		queue_free()
