extends Area2D

var parent
export (int) var damage = 20


func death():
	queue_free()
	
	
func _on_explosion_body_entered(body):
	if body.has_method("take_damage"):
		body.attacker = parent
		body.take_damage(damage)
		
