extends Node2D

var child
export var timeout = 0

export var items = [
	"res://objects/powerups/MedPack.tscn",
	"res://objects/powerups/MiniMedPack.tscn",
	"res://objects/powerups/soul_sphere.tscn",
	"res://objects/powerups/JumpBoster.tscn",

]

func _process(delta):
	if is_instance_valid(child):
		return
	
	timeout -= delta
	if timeout > 0:
		return

	randomize()
	timeout = rand_range(5,15)
	var item = items[randi()%items.size()]
	item = load(item).instance()
	item.position = position
	child = item
	get_tree().get_current_scene().add_child(item)
	
