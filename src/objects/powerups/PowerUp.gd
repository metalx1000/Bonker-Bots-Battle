extends KinematicBody2D

export var item = "MedPack"
export var health = 20
export var jump_boost = 1.0


export var pickup_snd = preload("res://objects/powerups/sounds/powerup.ogg")
var sound_obj = preload("res://objects/sound_item/sound.tscn")

export (int) var gravity = 1000
var velocity = Vector2()

func _physics_process(delta):
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)

func _on_Area2D_body_entered(body):
	if body.is_in_group("players"):
		if jump_boost > 1:
			body.jump_boost = jump_boost
			
		health_pickup(body)
		play_sound()
		queue_free()

func play_sound():
	var sound = sound_obj.instance()
	sound.position = position
	get_tree().get_current_scene().add_child(sound)
	sound.play_sound(pickup_snd)
	
func health_pickup(player):
	if health > 0 && player.health !=100:
		player.health += health
		if player.health > 100:
			player.health = 100
