extends KinematicBody2D

export var item = "rockets"
export var amount = 5
export var object = preload("res://objects/weapons/rocket_01/rocket.tscn")
export var pickup_snd = preload("res://objects/pickups/sounds/rocket_launcher.ogg")
export var shoot_delay = .5
export (int) var gravity = 1000



var velocity = Vector2()
func _ready():
	set_collision_layer_bit(1,false)
	pass # Replace with function body.

func _physics_process(delta):
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)

func _on_Area2D_body_entered(body):
	if body.is_in_group("players"):
		#check if item exists in player array
		var exists = false
		for i in body.weapons.size():
			if body.weapons[i].item == item:
				exists = true
				break
		
		if exists:
			update_player_item(body)
		else:
			add_player_item(body)

		queue_free()

func add_player_item(body):
	body.can_shoot = true
	var icon = $Sprite.texture
	body.weapons.append(
		{"item":item,
		"ammo":amount,
		"object": object,
		"delay":shoot_delay,
		"icon":icon}
		)

	body.current_weapon = body.weapons.size()-1
	update_player_item(body)
	
func update_player_item(body):
	for i in body.weapons.size():
		if body.weapons[i].item == item:
			body.weapons[i].ammo += amount
			body.current_weapon = i
			body.reload_snd.play()
			body.show_weapon()
