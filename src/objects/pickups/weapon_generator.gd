extends Node2D

var child

export var weapons = [
	"res://objects/pickups/rocket_launcher.tscn",
	"res://objects/pickups/chain_gun.tscn",
	"res://objects/pickups/bounce_bullet_gun.tscn",
	"res://objects/pickups/freeze_gun.tscn",
	"res://objects/farts/burrito.tscn",
	"res://objects/pickups/anti-gravity.tscn",
	"res://objects/pickups/shrink_ray.tscn",
	"res://objects/pickups/missile_launcher.tscn"
]

func _ready():
	pass # Replace with function body.



func _on_Timer_timeout():
	if is_instance_valid(child):
		return

	randomize()
	var weapon = weapons[randi()%weapons.size()]
	weapon = load(weapon).instance()
	weapon.position = position
	child = weapon
	get_tree().get_current_scene().add_child(weapon)
	$Timer.wait_time = rand_range(5,15)
	$Timer.start()
