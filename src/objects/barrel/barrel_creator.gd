extends Node2D

export var wait_time = 5
export var spin = 0
var barrels = preload("res://objects/barrel/barrel.tscn")


func _ready():
	$Timer.wait_time = wait_time


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	#if there are already more than 15 don't through grenade
	var count = get_tree().get_nodes_in_group("barrels").size()
	if count > 15:
		return
	var barrel = barrels.instance()
	randomize()
	var spin_direction = rand_range(-1,1)
	barrel.position = position
	barrel.angular_velocity = spin * spin_direction
	get_tree().get_current_scene().add_child(barrel)
