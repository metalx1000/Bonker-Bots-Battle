extends RigidBody2D

export var explosions = preload("res://objects/explosion/explosion_2.tscn")
var health = 10
var attacker 
var force_y = 0
var torque = 0
var dead = false

onready var hit_snds = [$snd_hit,$snd_hit2,$snd_hit3]

func _physics_process(delta):
	applied_torque += torque
	applied_force.y += force_y

func take_damage(amount):
	health -= amount
	if health < 1:
		death()
		
func death():
	if dead:
		return
	
	dead = true
	randomize()
	force_y = rand_range(-500,-200)
	torque = rand_range(-100,100)
	angular_velocity = rand_range(-5,5)
	force_y = -50
	torque = 50000
	gravity_scale = 1

	$DeathTimer.start()


func _on_DeathTimer_timeout():
	var explosion = explosions.instance()
	explosion.position = position
	get_tree().get_current_scene().add_child(explosion)
	queue_free()


func _on_Area2D_body_entered(body):
	randomize()
	hit_snds.shuffle()
	var snd = hit_snds[0]
	if !snd.playing:
		snd.play()
	
