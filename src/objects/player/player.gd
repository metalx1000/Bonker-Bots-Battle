extends KinematicBody2D
#Copyright Kris Occhipinti https://filmsbykris.com
#https://gitlab.com/metalx1000/Godot-Unlimited-Multiplayer
#Licensed under GPLv3
#https://gitlab.com/metalx1000/Godot-Unlimited-Multiplayer/-/blob/master/LICENSE

export (int) var player_id = 0
export (int) var speed = 3200
export (int) var jump_speed = -400
export (int,0,200) var inertia = 100
var jump_boost = 1

export (int) var gravity = 1000
var start_gravity = gravity
var start_speed = speed

onready var ray = $RayCast2D
onready var ray2 = $RayCast2D2
onready var reload_snd = $reload_snd
onready var status_text = $status/Label
onready var status_text_player = $status/label_fade
onready var weapon_icon_player = $status/icon_fade
onready var fart_damage = $fart_damage

var attacker
var kills = 0
var deaths = 0
var self_deaths = 0

var current_weapon = 0
var can_change_weapon = true
var frozen = false
var floating = false
export var shrunk = false

var velocity = Vector2.ZERO
onready var projectile_start_pos = $projectile_pos.position
var death_object = preload("res://objects/explosion/explosion.tscn")
var fart_object = preload("res://objects/farts/fart.tscn")

var weapons = []

var can_shoot = false
var health = 100
var dead = false

onready var sprite=$sprite

func _physics_process(delta):
	$HealthBar.value = health
	
	if frozen:
		return
		
	velocity.x = 0
	get_input(delta)
	if sprite.flip_h:
		$projectile_pos.position.x = -projectile_start_pos.x
	else:
		$projectile_pos.position.x = projectile_start_pos.x
		
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP,false,4,PI/4,false)
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.is_in_group("pushables"):
			if collision.collider.get("animationplayer") != null:
				collision.collider.animationplayer.play("death")
			collision.collider.apply_central_impulse(-collision.normal * inertia)
	outofbounds()


func set_color():
	var v = float(player_id*.2)
	# Duplicate the shader so that changing its param doesn't change it on any other sprites that also use the shader.
	# Generally done once in _ready()
	sprite.set_material(sprite.get_material().duplicate(true))

	# Offset sprite hue by a random value within specified limits.
	#var rand_hue = float(randi() % 3)/2.0/3.2
	sprite.material.set_shader_param("Shift_Hue", v)


func get_input(delta):
	var AXIS = Input.get_joy_axis(player_id,0)
	var DPAD_RIGHT = Input.is_joy_button_pressed(player_id,Controls.buttons.RIGHT)
	var DPAD_LEFT = Input.is_joy_button_pressed(player_id,Controls.buttons.LEFT)
	var left = Input.is_action_pressed("player_left")
	var right = Input.is_action_pressed("player_right")
	
	if AXIS > 0.5 || right || DPAD_RIGHT:
		sprite.flip_h = false
		sprite.play("walk")	
		velocity.x += speed * delta * 2
	if AXIS < -0.5 || left || DPAD_LEFT:
		sprite.flip_h = true
		sprite.play("walk")
		velocity.x -= speed * delta * 2
	
	if velocity.x < 30 && velocity.x > -30:
		sprite.play("default")
		
	if Input.is_action_just_pressed("jump")|| Input.is_joy_button_pressed(player_id,Controls.buttons.JUMP):
		if is_on_floor():
			if jump_boost > 1:
				$jump_boost.play()
			else:
				$jump_snd.play()
			velocity.y = jump_speed * jump_boost
	shoot()
	if Input.is_action_just_pressed("change_weapon")||Input.is_joy_button_pressed(player_id,Controls.buttons.CHANGE_WEAPON):
		change_weapon()
func shoot():
	
	if Input.is_action_pressed("shoot")|| Input.is_joy_button_pressed(player_id,Controls.buttons.SHOOT):
		if !can_shoot:
			return
		
		can_shoot = false
		load_projectile(weapons[current_weapon])
		status_text.text = str(weapons[current_weapon].ammo)
		status_text_player.stop()
		status_text_player.play("fade")
		
		$shoot_delay.start()
		
func load_projectile(weapon):
	$shoot_delay.wait_time = weapon.delay

	if weapon.ammo < 1:
		change_weapon()
		return

	weapon.ammo -= 1


	var projectile = weapon.object.instance()
	projectile.parent = self
	projectile.position = $projectile_pos.global_position
	
	if floating:
		projectile.position = $projectile_pos.position
		projectile.scale = Vector2(.5,.5)
		add_child(projectile)
	else:
		projectile.position = $projectile_pos.global_position
		get_tree().get_current_scene().add_child(projectile)
		
	if sprite.flip_h:
		projectile.direction = -1
		projectile.scale.x = -1
	
func _on_shoot_delay_timeout():
	can_shoot = true
	
func take_damage(amount):

	if !$voice.playing:
		var voice = load("res://objects/player/sounds/voice_hurt_01.ogg")
		voice.set_loop(false)
		$voice.stream = voice
		$voice.play()
	$AnimationPlayer.play("damage")
	health -= amount

	if health <= 0 || shrunk && amount>0:
		death()

func outofbounds():
	if !dead && position.y > 1000:
		death()

func death():
	if dead:
		return
	dead = true
	
	deaths += 1
	
	if attacker == self:
		self_deaths += 1
	else:
		if attacker != null:
			attacker.kills += 1
	
	$respawn_timer.start()
	var death = death_object.instance()
	death.position = global_position
	death.scale = Vector2(.5,.5)
	get_tree().get_current_scene().add_child(death)
	position = Vector2(-10000,-10000)
	reset_items()

func change_weapon():
	if !can_change_weapon:
		return
	
	can_change_weapon = false
	$Change_Weapon_Timer.start()
	$reload_snd.play()
	current_weapon += 1

	if current_weapon >= weapons.size():
		current_weapon = 0
	show_weapon()
	
func show_weapon():
	weapon_icon_player.stop()
	weapon_icon_player.play("fade")
	
	if weapons.size() != 0 && weapons[current_weapon].icon != null:
		$status/weapon.texture = weapons[current_weapon].icon
	
func reset_items():
	$shrink.play("RESET")
	for i in weapons.size():
		weapons[i].ammo = 0

func _on_respawn_timer_timeout():
	$spawn_snd.play()
	position = Global.random_start_pos()
	health = 100
	jump_boost = 1
	dead = false

func freeze():
	$freeze_snd.play()
	$frozen_timer.start()
	frozen = true
	$sprite.play("frozen")

func unfreeze():
	frozen = false

func fart_start():
	$Fart_Stop_Timer.start()
	$Fart_Timer.start()
	
func fart():
	var fart = fart_object.instance()
	fart.parent = self
	get_tree().get_current_scene().add_child(fart)
	fart.position = position

func _on_Fart_Stop_Timer_timeout():
	$Fart_Timer.stop()

func fart_take_damage():
	take_damage(3)
	
func _on_Change_Weapon_Timer_timeout():
	can_change_weapon = true

func gravity_float():
	floating = true
	$Gravity_Timer.start()
	gravity = -10
	speed *= .5
	$rotation_player.play("rotate")

func _on_Gravity_Timer_timeout():
	floating = false
	gravity = start_gravity
	speed = start_speed

func shrink():
	$shrink.play("shrink")

