extends Area2D

var parent

func _ready():
	pass # Replace with function body.


func _on_shrink_ray_hit_body_entered(body):
	if body.is_in_group("players"):
		body.shrink()
