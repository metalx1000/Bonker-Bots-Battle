extends RigidBody2D

onready var animationplayer = $AnimationPlayer
export var explosions = preload("res://objects/explosion/explosion_2.tscn")

onready var hit_snds = [$snd_hit,$snd_hit2,$snd_hit3]

func _ready():
	pass # Replace with function body.



func death():
	var explosion = explosions.instance()
	explosion.position = position
	get_tree().get_current_scene().add_child(explosion)
	queue_free()

func _on_Area2D_body_entered(body):
	randomize()
	hit_snds.shuffle()
	var snd = hit_snds[0]
	if !snd.playing:
		snd.play()
	
