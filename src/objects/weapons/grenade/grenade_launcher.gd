extends Node2D

onready var timer = $Timer
export var direction_updown = 1
export var updown_min = -100
export var updown_max = 200

export var direction_leftright = 1
export var leftright_min = 200
export var leftright_max = 500

export var grenades = preload("res://objects/weapons/grenade/grenade.tscn")

func _ready():
	randomize()
	timer.wait_time = rand_range(3,8)
	timer.start()
	

func shoot():
	#if there are already more than 15 don't through grenade
	var count = get_tree().get_nodes_in_group("grenades").size()
	if count > 15:
		return
		
	var grenade = grenades.instance()
	grenade.position = position
	get_tree().get_current_scene().add_child(grenade)
	grenade.angular_velocity = 10
	
	var y = rand_range(updown_min,updown_max) * direction_updown
	var x = rand_range(leftright_min,leftright_min) * direction_leftright
	grenade.apply_impulse(Vector2(),Vector2(x,y))
	
