extends Area2D

var direction = 1
export var direction_y = 0
export var speed = 200
export var bounce = false
var parent

onready var sprite = $sprite
export var explosions = preload("res://objects/explosion/explosion_2.tscn")
export var smokes = preload("res://objects/smoke-fire/smoke-red.tscn")
export var smoke_enabled = true
export var explodes = true
export var dead = false

export (int) var damage = 0

var smoke

func _ready():
	if bounce:
		randomize()
		direction_y = rand_range(-100,100) 
	$sprite.play("default")
	if smoke_enabled:
		create_smoke()

func _process(delta):
	position.x += speed * direction * delta
	position.y += direction_y * delta

func create_smoke():
	smoke = smokes.instance()
	smoke.parent = self
	get_tree().get_current_scene().add_child(smoke)

func _on_rocket_body_entered(body):
	if body.is_in_group("maps") && bounce:
		return
		
	#if body == parent:
		#return
	if dead:
		return
		
	dead = true
		
	if smoke_enabled:
		smoke.active = false
	
	if explodes:
		var explosion = explosions.instance()
		explosion.parent = parent
		explosion.position = global_position
		get_tree().get_current_scene().add_child(explosion)
	
	if body.has_method("take_damage"):
		body.attacker = parent
		body.take_damage(damage)

	start_death()
	
func start_death():
#	$CollisionShape2D.disabled = true
	visible = false
	$Death_Timer.wait_time = .5
	$Death_Timer.start()
	

func _on_updown_body_entered(body):
	if body.is_in_group("maps"):
		$bounce_snd.play()
		direction_y *= -1


func _on_leftright_body_entered(body):
	if body.is_in_group("maps"):
		$bounce_snd.play()
		direction *= -1

func death():
	queue_free()


func _on_freeze_missile_body_entered(body):
	if body.is_in_group("players"):
		body.freeze()
		
	start_death()
