extends Area2D

var direction = 1
export var direction_y = 0
export var speed = 100
export var bounce = false
var parent

onready var sprite = $sprite
export var explosions = preload("res://objects/explosion/explosion_2.tscn")
export var smokes = preload("res://objects/smoke-fire/smoke-red.tscn")
export var smoke_enabled = true
export var explodes = true
export var dead = false

export (int) var damage = 0

var smoke

func _ready():
	$sprite.play("default")
	if smoke_enabled:
		create_smoke()

func _process(delta):
	get_input()
	position.x += speed * direction * delta

	position.y += direction_y * delta * 50

func create_smoke():
	smoke = smokes.instance()
	smoke.parent = self
	get_tree().get_current_scene().add_child(smoke)

func _on_rocket_body_entered(body):
	if body.is_in_group("maps") && bounce:
		return
		
	#if body == parent:
		#return
	if dead:
		return
		
	dead = true
		
	if smoke_enabled:
		smoke.active = false
	
	if explodes:
		var explosion = explosions.instance()
		explosion.parent = parent
		explosion.position = global_position
		get_tree().get_current_scene().add_child(explosion)
	
	if body.has_method("take_damage"):
		body.attacker = parent
		body.take_damage(damage)

	start_death()

func get_input():
	var player_id = parent.player_id
	var AXIS = Input.get_joy_axis(player_id,0)
	var AXIS_1 = Input.get_joy_axis(player_id,1)
	var DPAD_RIGHT = Input.is_joy_button_pressed(player_id,Controls.buttons.RIGHT)
	var DPAD_LEFT = Input.is_joy_button_pressed(player_id,Controls.buttons.LEFT)
	
	var DPAD_UP = Input.is_joy_button_pressed(player_id,Controls.buttons.UP)
	var DPAD_DOWN = Input.is_joy_button_pressed(player_id,Controls.buttons.DOWN)
	
	var left = Input.is_action_pressed("player_left")
	var right = Input.is_action_pressed("player_right")
	var up = Input.is_action_pressed("player_up")
	var down = Input.is_action_pressed("player_down")
	
	if AXIS > 0.5 || right || DPAD_RIGHT:
		direction = 1
		sprite.flip_h = false
	if AXIS < -0.5 || left || DPAD_LEFT:
		direction = -1
		sprite.flip_h = true

	if AXIS_1 < -0.5 || up || DPAD_UP:
		direction_y = -1
	elif AXIS_1 > 0.5 || down || DPAD_DOWN:
		direction_y = 1
	else:
		direction_y = 0

	
func start_death():
#	$CollisionShape2D.disabled = true
	visible = false
	$Death_Timer.wait_time = .5
	$Death_Timer.start()
	

func death():
	queue_free()


func _on_freeze_missile_body_entered(body):
	if body.is_in_group("players"):
		body.freeze()
		
	start_death()
