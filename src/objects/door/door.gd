extends KinematicBody2D

export var open = false
export var time_min = 3.0
export var time_max = 5.0

func _ready():
	randomize()
	$Timer.wait_time = rand_range(time_min,time_max)

func activate():
	if open:
		open = false
		$open.play()
		$AnimationPlayer.play("up")
	else:
		open = true
		$close.play()
		$AnimationPlayer.play_backwards("up")

