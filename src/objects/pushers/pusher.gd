extends Area2D

export var direction_x = 1
var items = []
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	for i in items:
		i.position.x += direction_x


func _on_pusher_body_entered(body):
	if body.is_in_group("pushables"):
		items.append(body)


func _on_pusher_body_exited(body):
	if body.is_in_group("pushables"):
		items.erase(body)
