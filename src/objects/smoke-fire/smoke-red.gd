extends Particles2D

var parent
var active = true

func _ready():
	pass # Replace with function body.



func _process(_delta):
	
	if active:
		position = parent.position
	else:
		$Timer.start()
		emitting = false


func _on_Timer_timeout():
	queue_free()
