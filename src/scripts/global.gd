extends Node

#Copyright Kris Occhipinti https://filmsbykris.com
#https://gitlab.com/metalx1000/Bonker-Bots-Battle
#Licensed under GPLv3
#https://gitlab.com/metalx1000/Bonker-Bots-Battle/-/blob/master/LICENSE

var player_clone = preload("res://objects/player/player.tscn")

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().change_scene("res://scenes/Title_Menu.tscn")
		
func _input(event):
	var id = event.device
	load_players(id)

	
func load_players(player_id):
	#check if there is a player start position
	if get_tree().get_nodes_in_group("player_start").size() < 1:
		return

	#if it's is not the jump button 
	#or left/right movement
	#being pressed don't continue
	var lr = abs(Input.get_joy_axis(player_id,0))
	var DPAD_RIGHT = Input.is_joy_button_pressed(player_id,Controls.buttons.RIGHT)
	var DPAD_LEFT = Input.is_joy_button_pressed(player_id,Controls.buttons.LEFT)
	var jump_btn = Input.is_joy_button_pressed(player_id,Controls.buttons.JUMP)
	var keyboard = Input.is_action_just_pressed("ui_select")
	
	if DPAD_RIGHT||DPAD_LEFT||jump_btn|| lr > 0.5 || keyboard:
		
		#check and make sure that this player doesn't already exist
		var players = get_tree().get_nodes_in_group("players")
		for player in players:
			if player.player_id == player_id:
				return

		var clone = player_clone.instance()
		clone.player_id = player_id
		get_tree().get_current_scene().add_child(clone)
		clone.set_color()
		clone.position = random_start_pos()

func random_start_pos():
	randomize()
	var positions = get_tree().get_nodes_in_group("player_start")
	var pos = positions[randi() % positions.size()]
	pos = pos.position
	return pos

func search_files(path,ext):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
 
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif file.ends_with(ext):
			print(file)
			files.append(file)
	dir.list_dir_end()
	return files
