extends Node2D

var winner
onready var text_screen = $intro/Text_Screen

export var map_title = ""

var tracks = [
	"res://music/Analog_Mechanic.ogg", "res://music/Flaming_Skulls.ogg", "res://music/Mud_Fight.ogg", "res://music/Off_the_Rails.ogg", "res://music/Until_Your_Engine_Stops_Manifold.ogg"
]

func _ready():
	$intro.visible = true
	intro_text()
	$HUD.visible = true
	$HUD/middle_msg.text = ""
	random_track()
	
func _process(_delta):
	check_winner()
	var key = Input.is_action_just_pressed("change_music_track")
	var button = Input.is_joy_button_pressed(0,Controls.buttons.CHANGE_MUSIC)
	if key || button:
		random_track()

func intro_text():
	var text = text_screen.get_node("text")

	text.text = "Entering\n"
	text.text += map_title
	
func random_track():
	randomize()
	var track_num = int(rand_range(0,tracks.size()))
	#var track = "res://music/"+tracks[track_num]
	var track = tracks[track_num]

	if ResourceLoader.exists(track):
		$music.stream = ResourceLoader.load(track)
		$music.play()
		

func check_winner():
	if winner != null:
		return

	var players = get_tree().get_nodes_in_group("players")

	for i in players.size():
		if players[i].kills >= 10:
			winner = players[i]
			var id = players[i].player_id + 1
			$HUD/middle_msg.text = "Player #" + str(id) + " WINS!!!"
			$HUD/AnimationPlayer.play("end_game")
	
	
func _unhandled_input(event):
	if event is InputEventJoypadButton:
		#print(event.button_index)
		if event.button_index == 11 && event.is_pressed():
			random_track()

