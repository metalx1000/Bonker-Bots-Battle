#!/bin/bash
###################################################################### 
#Copyright (C) 2023  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

[[ $1 ]] && audio="$*" || audio=$(fzf --prompt="Select an Audio File: ")
[[ $audio ]] || exit

tmp_file_1="robotvoice_tmp_01.ogg"
tmp_file_2="robotvoice_tmp_02.ogg"
pitch_01="0.9"
pitch_02="1.2"

ffmpeg -i "$audio" -af "asetrate=44100*$pitch_01,aresample=44100,atempo=1/$pitch_01" $tmp_file_1 
ffmpeg -i "$audio" -af "asetrate=44100*$pitch_02,aresample=44100,atempo=1/$pitch_02" $tmp_file_2
ffmpeg -i "$audio" -i $tmp_file_1 -i $tmp_file_2 -filter_complex "[0:a][1:a][2:a]amix=3[a]" -ac 2 -map "[a]"  "voice_$audio"

rm "$tmp_file_1" "$tmp_file_2"
mpv "voice_$audio"
